import { TestBed, inject } from '@angular/core/testing';

import { ProgresRepositoryService } from './progres-repository.service';

describe('ProgresRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProgresRepositoryService]
    });
  });

  it('should be created', inject([ProgresRepositoryService], (service: ProgresRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
