# Puzle Journalist

## About

With Puzle Journalist, we wrap your daily news website into a single web app.

[WIP]

The project is currently in WIP. Now, you can do nothing with it. Be patient, we will be there soon =D

## Stack

- Ionic
- Angular
- Karma
- Protractor

## Roadmap

- Master / detail app
- Data cache: enable the offline reading

## Copy / Clone

```sh
$git clone https://gitlab.com/puzle-project/journalist.git
```

## Installation

```sh
$yarn install
```

## Test

```sh
$yarn run test
```

## E2E

To run the end To end tests:

```sh
$yarn run e2e
```

## Lint

```sh
$yarn run lint
```

## Build

```sh
$yarn run build
```

